CREATE SCHEMA GSA_ERP;

USE GSA_ERP;

CREATE TABLE association (
	id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    address TEXT NULL DEFAULT NULL
);

CREATE TABLE event (
	id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    start_date DATE NULL DEFAULT NULL,
    end_date DATE NULL DEFAULT NULL,
    location TEXT NULL DEFAULT NULL,
    description TEXT NULL DEFAULT NULL
);

CREATE TABLE event_association (

	event INT NOT NULL,
    association INT NOT NULL
);

CREATE TABLE activity (

	id INT PRIMARY KEY AUTO_INCREMENT,
    event INT NOT NULL,
    name VARCHAR(255) NOT NULL,
    minimum_capacity INT NOT NULL DEFAULT 0,
    maximum_capacity INT NOT NULL DEFAULT 0
);

CREATE TABLE user (

	id INT PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    password CHAR(60) NOT NULL
);

CREATE TABLE shift (

	id INT PRIMARY KEY AUTO_INCREMENT,
    start_time TIME NOT NULL,
    end_time TIME NOT NULL
);

CREATE TABLE user_association (

	user INT NOT NULL,
    association INT NOT NULL
);

CREATE TABLE affectation (

	user INT NOT NULL,
    shift INT NOT NULL,
    activity INT NOT NULL
);