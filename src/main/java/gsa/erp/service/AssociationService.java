package gsa.erp.service;

import gsa.erp.model.Association;
import gsa.erp.model.User;
import gsa.erp.model.repository.AssociationRepository;
import gsa.erp.model.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

@Service("associationService")
public class AssociationService {

    @Inject
    private AssociationRepository associationRepository;
    @Inject
    private UserRepository userRepository;

    public List<Association> getAll() {
        return associationRepository.findAll();
    }

    public void addMember(Association association, User user) {

        Set<User> members = association.getMembers();
        if (!members.add(user)) {
            throw new IllegalArgumentException("User already in association.");
        }
        associationRepository.save(association);
    }

    public void removeMember(Association asso, User user) {

        Set<User> assoMembers = asso.getMembers();
        if (!assoMembers.contains(user)) {
            throw new IllegalArgumentException("User is not in association.");
        }
        assoMembers.remove(user);
        associationRepository.save(asso);
    }

    public Association getByName(String nomAsso) {
        List<Association> assos = this.getAll();
        Association asso = null;
        for (Association association : assos) {
            if (association.getName().equalsIgnoreCase(nomAsso)) {
                asso = association;
            }
        }
        return asso;
    }

    public Association getAssociation(int id) {
        return this.associationRepository.findById(id);
    }
}
