package gsa.erp.service;

import gsa.erp.model.*;
import gsa.erp.model.repository.ActivityRepository;
import gsa.erp.model.repository.AffectationRepository;
import gsa.erp.model.repository.ShiftRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

@Service("planningService")
public class PlanningService {

    @Inject
    private AffectationRepository affectationRepository;
    @Inject
    private ActivityRepository activityRepository;
    @Inject
    private ShiftRepository shiftRepository;
    @Inject
    private UserService userService;

    public Map<Activity, Map<Shift, List<User>>> getFormattedPlanning(Event event) {
        final Set<Activity> activities = this.getActivities(event);
        final Set<Shift> shifts = this.getShifts(event);
        final Set<Affectation> rawPlanning = getRawPlanning(event);

        final Map<Activity, Map<Shift, List<User>>> formattedPlanning = new HashMap<>();

        for (Activity activity : activities) {
            HashMap<Shift, List<User>> activityAffectations = new HashMap<>();
            formattedPlanning.put(activity, activityAffectations);
            for (Shift shift : shifts) {
                activityAffectations.put(shift, new ArrayList<>());
            }
        }

        for (Affectation affectation : rawPlanning) {
            formattedPlanning.get(affectation.getActivity()).get(affectation.getShift()).add(affectation.getUser());
        }

        return formattedPlanning;
    }

    public Map<Activity, Map<Shift, List<User>>> getFormattedPersonnalPlanning(Event event) {
        final Set<Activity> activities = this.getActivities(event);
        final Set<Shift> shifts = this.getShifts(event);
        final Set<Affectation> rawPlanning = getRawPlanning(event);

        final Map<Activity, Map<Shift, List<User>>> formattedPlanning = new HashMap<>();

        for (Activity activity : activities) {
            HashMap<Shift, List<User>> activityAffectations = new HashMap<>();
            formattedPlanning.put(activity, activityAffectations);
            for (Shift shift : shifts) {
                activityAffectations.put(shift, new ArrayList<>());
            }
        }

        for (Affectation affectation : rawPlanning) {
            formattedPlanning.get(affectation.getActivity()).get(affectation.getShift()).add(affectation.getUser());
        }


        for (Map.Entry<Activity, Map<Shift, List<User>>> entrySet : formattedPlanning.entrySet()) {
            for (Map.Entry<Shift, List<User>> shiftsEntrySet : entrySet.getValue().entrySet()) {
                if (shiftsEntrySet.getValue().isEmpty()) {
                    shiftsEntrySet.getValue().add(new User(-1, "", "", ""));
                }
                for (User u : shiftsEntrySet.getValue()) {
                    if (!u.equals(this.userService.getCurrentUser())) {
                        u.setLastName("---");
                        u.setFirstName("---");
                    }
                }
            }
        }


        return formattedPlanning;
    }

    private Set<Affectation> getRawPlanning(Event event) {
        return affectationRepository.findByActivityEvent(event);
    }

    private Set<Shift> getShifts(Event event) {
        return this.shiftRepository.findByEvent(event);
    }

    private Set<Activity> getActivities(Event event) {
        return this.activityRepository.findByEvent(event);
    }
}
