package gsa.erp.service;

import gsa.erp.model.Association;
import gsa.erp.model.User;
import gsa.erp.model.UserAssociation;
import gsa.erp.model.repository.UserAssociationRepository;
import gsa.erp.model.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Service("userService")
public class UserService {

    public static final int BCRYPT_LOG_ROUNDS = 12;
    @Inject
    private UserRepository userRepository;
    @Inject
    private UserAssociationRepository userAssociationRepository;
    @Inject
    private AuthService authService;

    private User currentUser = null;
    private Association currentAssociation = null;

    @Transactional
    public boolean checkCredentials(Association association, String username, String password) {
        List<User> userList = association.getMembers().stream().filter(
                user -> user.getUsername().equalsIgnoreCase(username)
                        && authService.checkPlainPassword(password, user.getPassword())).collect(Collectors.toList());
        if (userList.size() != 1) {
            return false;
        }

        boolean result = !StringUtils.isAnyBlank(username, password);
        this.currentUser = userList.get(0);
        this.currentAssociation = association;
        return result;
    }

    @Transactional
    public boolean exists(String firstName, String lastName) {
        return userRepository.findByFirstNameAndLastName(firstName.toLowerCase(), lastName.toLowerCase()).size() != 0;
    }

    @Transactional
    public boolean exists(int id) {
        return userRepository.findById(id) != null;
    }

    @Transactional
    public boolean create(Association association, String rankLabel, String firstName, String lastName, String password) {
        User newUser = new User();
        newUser.setFirstName(firstName.toLowerCase());
        newUser.setLastName(lastName.toLowerCase());
        this.setClearPassword(newUser, password);

        UserAssociation newUserAssociation = new UserAssociation();
        newUserAssociation.setAssociation(association);
        newUserAssociation.setUser(newUser);
        UserAssociation.Rank rank = UserAssociation.Rank.getByLabel(rankLabel);
        if (rank == null) {
            throw new IllegalArgumentException("unknown rankLabel " + rankLabel);
        }
        newUserAssociation.setRank(rank);
        newUser.getUserAssociations().add(newUserAssociation);
        userRepository.save(newUser);
        userAssociationRepository.save(newUserAssociation);

        return exists(firstName, lastName);
    }

    @Transactional
    public User getUser(int id) {
        if (userRepository.findById(id) != null) {
            return userRepository.findById(id);
        } else {
            throw new IllegalArgumentException("User does not exists !\n");
        }

    }

    private void setClearPassword(User user, String clearPassword) {
        user.setPassword(BCrypt.hashpw(clearPassword, BCrypt.gensalt(BCRYPT_LOG_ROUNDS)));
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public Association getCurrentAssociation() {
        return currentAssociation;
    }
}
