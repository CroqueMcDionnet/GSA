package gsa.erp.service;

import gsa.erp.model.Event;
import gsa.erp.model.repository.EventRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
@Service("eventService")
public class EventService {

    @Inject
    private EventRepository eventRepository;

    public List<Event> findAll() {
        return eventRepository.findAll();
    }

    public List<Event> findAllSortByStartDate() {
        return eventRepository.findAllByOrderByStartDate();
    }

    public boolean isValid(Event event) {
        return (event.getStartDate().isBefore(event.getEndDate()));
    }

    public void addEvent(Event event){
         eventRepository.save(event);
    }
        /*
                Set<User> assoMembers = asso.getMembers();
        if(assoMembers.contains(user)) {
            throw new IllegalArgumentException("User already in association.");
        }
        assoMembers.add(user);
        associationRepository.save(asso);
         */
}
