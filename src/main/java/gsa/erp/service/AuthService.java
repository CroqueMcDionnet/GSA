package gsa.erp.service;

import org.apache.commons.lang3.StringUtils;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;

@Service("authService")
public class AuthService {

    public boolean checkPlainPassword(String plainPassword, String hashedPassword) {
        if (StringUtils.isAnyBlank(plainPassword, hashedPassword)) {
            throw new IllegalArgumentException("passwords can not be blank");
        }

        return BCrypt.checkpw(plainPassword, hashedPassword);
    }
}
