package gsa.erp.controller;

import gsa.erp.model.Event;
import gsa.erp.model.repository.TodoRepository;
import gsa.erp.service.EventService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import tornadofx.control.DateTimePicker;

import java.io.IOException;
import java.net.URL;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;

@Component
public class EventFormController {

    @Autowired
    private EventService eventService;

    @Autowired
    private TodoRepository todoRepository;

    @Autowired
    private ApplicationContext context;

    private Stage currentStage;

    private Event currentEvent;

    @FXML
    private Button addEditButton;

    @FXML
    private TextField eventName;

    @FXML
    private DateTimePicker eventStartDate;

    @FXML
    private DateTimePicker eventEndDate;

    @FXML
    private TextArea eventDescription;

    @FXML
    private TextArea eventAddress;

    @FXML
    public void initialize() {
        if(currentEvent != null){
            eventAddress.setText(currentEvent.getLocation());
            eventDescription.setText(currentEvent.getDescription());
            eventStartDate.setDateTimeValue(currentEvent.getStartDate());
            eventEndDate.setDateTimeValue(currentEvent.getEndDate());
            eventName.setText(currentEvent.getName());
        }
    }

    @FXML
    public void addOrEditEvent(ActionEvent actionEvent) throws IOException{
        Event eventAdd;
        eventAdd = new Event();

        if(eventName.getText()==null || eventName.getText().isEmpty()){
            ShowErrorEventDialog("nom","nom","Le nom doit être une chaine de caractère");
        } else{
            eventAdd.setName(eventName.getText());
            if(!(eventStartDate.getValue()!=null && isLegalDate(eventStartDate.getValue().toString()) && eventEndDate.getValue()!=null && isLegalDate(eventEndDate.getValue().toString()))){
                ShowErrorEventDialog("date","date","Vérifier que la date de début et la date de fin soit correcte");
            }else{
                eventAdd.setStartDate(eventStartDate.getDateTimeValue());
                eventAdd.setEndDate(eventEndDate.getDateTimeValue());
                if(eventDescription!= null || (!eventDescription.getText().isEmpty()))
                {
                    eventAdd.setDescription(eventDescription.getText());
                }
                if(eventAddress!=null || (!eventAddress.getText().isEmpty())){
                    eventAdd.setLocation(eventAddress.getText());
                }
                eventService.addEvent(eventAdd);
                resetValue();
                returnToMainStage(actionEvent);
            }
        }
    }

    @FXML
    public void showAddOrEditForm(ApplicationContext context, Stage currentStage, Event event) throws IOException {
        this.context = context;
        this.currentEvent = event;
        URL url = getClass().getResource("/gsa/erp/view/addOrEditEvent.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        fxmlLoader.setControllerFactory(context::getBean);
        Parent rootNode = fxmlLoader.load();

        this.currentStage = currentStage;
        this.currentStage.setTitle("GSA - Ajout d'évènement");
        Scene scene = new Scene(rootNode);
        currentStage.setScene(scene);
        currentStage.show();
        /*
        eventAddress.setText(event.getLocation());
        eventDescription.setText(event.getDescription());
        eventStartDate.setDateTimeValue(event.getStartDate());
        eventEndDate.setDateTimeValue(event.getEndDate());
        eventName.setText(event.getName());
        */
    }

    @FXML
    public void showAddOrEditForm(ApplicationContext context, Stage currentStage) throws IOException {
        this.showAddOrEditForm(context, currentStage, new Event());
    }

    @FXML
    private void cancelAddOrEditEvent(ActionEvent actionEvent) throws IOException{
        resetValue();
        returnToMainStage(actionEvent);

    }

    private boolean isLegalDate(String s) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);
        return sdf.parse(s, new ParsePosition(0)) != null;
    }

    private void ShowErrorEventDialog(String errorName,String errorHeader,String errorContent){
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Erreur évènement - "+errorName);
        alert.setHeaderText("Veuillez entrer un "+errorHeader+" correcte.");
        alert.setContentText(errorContent);
        alert.showAndWait();
    }

    private void resetValue(){
        eventAddress.setText(null);
        eventDescription.setText(null);
        eventStartDate.setDateTimeValue(null);
        eventEndDate.setDateTimeValue(null);
    }

    private void returnToMainStage(ActionEvent actionEvent) throws IOException{
        Stage currentStage = (Stage) ((Button) actionEvent.getSource()).getScene().getWindow();
        new HomeController().showHomePage(context, currentStage);
    }
}
