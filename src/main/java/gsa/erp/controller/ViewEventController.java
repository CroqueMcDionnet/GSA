package gsa.erp.controller;

import gsa.erp.model.*;
import gsa.erp.service.PlanningService;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.IOException;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ViewEventController {

    @FXML private Label eventDates;
    @FXML private Label personnaleventDates;
    @FXML private Label eventAddress;
    @FXML private Label personnaleventAddress;
    @FXML private Button modifyEvent;
    @FXML private Button personnalmodifyEvent;
    @FXML private Button backHomeButton;
    @FXML private Button personnalbackHomeButton;
    @FXML private Label eventDescription;
    @FXML private Label personnaleventDescription;
    @FXML private Label eventName;
    @FXML private Label personnaleventName;
    @FXML private Tab planningTab;
    @FXML private Tab personnalPlanningTab;
    private ApplicationContext context;
    private Stage stage;
    private Event event = null;

    @Inject
    private PlanningService planningService;
    private TableView<PlanningRow> planningTableView;
    private TableView<PlanningRow> personnalPlanningTableView;

    public ViewEventController() {

    }

    public  void initialize() {
        eventName.setText(this.event.getName());
        eventDescription.setText(this.event.getDescription());
        eventDates.setText("Du " + this.event.getStartDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")) + " au "
                + this.event.getEndDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
        eventAddress.setText("Adresse : " + this.event.getLocation());

        this.initializePersonnalPlanning();
        this.initializePlanning();
    }

    private void initializePersonnalPlanning() {
        Map<Activity, Map<Shift, List<User>>> planning = this.planningService.getFormattedPersonnalPlanning(event);

        final Set<Shift> shifts = new HashSet<>();
        for (Map<Shift, List<User>> planningEntry : planning.values()) {
            shifts.addAll(planningEntry.keySet());
        }
        personnalPlanningTableView = new TableView<>();

        List<TableColumn<PlanningRow, String>> columns = new ArrayList<>();
        int i = 0;
        for (Shift shift : shifts) {
            final int j = i++;
            TableColumn<PlanningRow, String> planningColumn = new TableColumn<>(shift.toString());
                    planningColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getShiftAffectations().get(j)));
            columns.add(planningColumn);
        }

        ObservableList<PlanningRow> rows = FXCollections.observableArrayList();

        planning.forEach((activity, shiftListMap) -> {
            ObservableList<String> shiftAffectations = FXCollections.observableArrayList();
            shiftListMap.forEach((shift, users) -> shiftAffectations.add(users.stream()
                    .map(User::getFullName)
                    .collect(Collectors.joining("\n"))));
            rows.add(new PlanningRow(new SimpleStringProperty(activity.getName()), shiftAffectations));
        });
        TableColumn<PlanningRow, String> activityColumn = new TableColumn<>("Activité");
        activityColumn.setCellValueFactory(new PropertyValueFactory("activityName"));
        personnalPlanningTableView.getColumns().add(activityColumn);
        personnalPlanningTableView.getColumns().addAll(columns);
        personnalPlanningTableView.setItems(rows);
        personnalPlanningTab.setContent(personnalPlanningTableView);
    }

    private void initializePlanning() {
        Map<Activity, Map<Shift, List<User>>> planning = this.planningService.getFormattedPlanning(event);

        final Set<Shift> shifts = new HashSet<>();
        for (Map<Shift, List<User>> planningEntry : planning.values()) {
            shifts.addAll(planningEntry.keySet());
        }

        planningTableView = new TableView<>();

        List<TableColumn<PlanningRow, String>> columns = new ArrayList<>();
        int i = 0;
        for (Shift shift : shifts) {
            final int j = i++;
            TableColumn<PlanningRow, String> planningColumn = new TableColumn<>(shift.toString());
            planningColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getShiftAffectations().get(j)));
            columns.add(planningColumn);
        }

        ObservableList<PlanningRow> rows = FXCollections.observableArrayList();

        planning.forEach((activity, shiftListMap) -> {
            ObservableList<String> shiftAffectations = FXCollections.observableArrayList();
            shiftListMap.forEach((shift, users) -> shiftAffectations.add(users.stream()
                    .map(User::getFullName)
                    .collect(Collectors.joining("\n"))));
            rows.add(new PlanningRow(new SimpleStringProperty(activity.getName()), shiftAffectations));
        });
        TableColumn<PlanningRow, String> activityColumn = new TableColumn<>("Activité");
        activityColumn.setCellValueFactory(new PropertyValueFactory("activityName"));
        planningTableView.getColumns().add(activityColumn);
        planningTableView.getColumns().addAll(columns);
        planningTableView.setItems(rows);
        planningTab.setContent(planningTableView);
    }

    @FXML
    public synchronized void showEventWindow(ApplicationContext context, Stage currentStage, Event event) throws IOException {
        this.context = context;
        this.event = event;
        URL url = getClass().getResource("/gsa/erp/view/viewEvent.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        fxmlLoader.setControllerFactory(context::getBean);
        Parent rootNode = fxmlLoader.load();

        stage = currentStage;
        stage.setTitle("Affichage de l'événement");
        Scene scene = new Scene(rootNode);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void returnToMainStage(ActionEvent actionEvent) throws IOException{
        Stage currentStage = (Stage) ((Button) actionEvent.getSource()).getScene().getWindow();
        new HomeController().showHomePage(context, currentStage);
    }

    public final class PlanningRow {
        private final SimpleStringProperty activityName;
        private final ObservableList<String> shiftAffectations;

        public PlanningRow(SimpleStringProperty activityName, ObservableList<String> shiftAffectations) {

            this.activityName = activityName;
            this.shiftAffectations = shiftAffectations;
        }

        public String getActivityName() {
            return activityName.get();
        }

        public SimpleStringProperty activityNameProperty() {
            return activityName;
        }

        public ObservableList<String> getShiftAffectations() {
            return shiftAffectations;
        }
    }
}
