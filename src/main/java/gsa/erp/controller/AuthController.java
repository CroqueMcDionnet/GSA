package gsa.erp.controller;

import gsa.erp.model.Association;
import gsa.erp.model.UserAssociation;
import gsa.erp.service.AssociationService;
import gsa.erp.service.UserService;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import java.util.stream.Collectors;

@Component
public class AuthController {
    private static final String BAD_CREDENTIALS_MESSAGE = "Vous ne faites pas partie de cette association ou vos identifiants sont erronés";
    private static final String EMPTY_REQUEST = "Tout les champs sont obligatoires, merci de les renseigner avant de valider";
    private static final String USER_ALREADY_EXIST = "Un utilisateur avec votre nom et votre prénom exise déjà en base de données";
    private static final String CREATION_FAILED = "Nous n'avons pas réussi a créer votre compte, contactez un administrateur.";
    private static final String NO_ASSOCIATION_SELECTED_MESSAGE = "Vous n'avez pas sélectionné d'association";
    private Stage currentStage;
    @Autowired
    private AssociationService associationService;

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationContext context;

    @FXML
    ChoiceBox associationChoiceBox;

    @FXML
    ChoiceBox rankChoiceBox;

    @FXML
    Button logInButton;

    @FXML
    Button signInButton;

    @FXML
    Button showAuthButton;

    @FXML
    Button showSignInButton;

    @FXML
    TextField usernameField;

    @FXML
    PasswordField passwordField;

    @FXML
    TextField firstNameField;

    @FXML
    TextField lastNameField;

    @FXML
    PasswordField createPasswordField;

    @FXML
    ImageView logo;


    @FXML
    public void setAssociationsData() {
        List<String> associationNames = associationService.getAll().stream()
                .map(Association::getName)
                .collect(Collectors.toList());
        associationChoiceBox.setItems(FXCollections.observableArrayList(associationNames));
        associationChoiceBox.getSelectionModel().selectFirst();
    }

    @FXML
    public void setRankData() {
        List<String> rankNames = new ArrayList<>();
        for(UserAssociation.Rank rank : UserAssociation.Rank.values()){
            rankNames.add(rank.getLabel());
        }
        rankChoiceBox.setItems(FXCollections.observableArrayList(rankNames));
        rankChoiceBox.getSelectionModel().selectFirst();
    }

    @FXML
    public void setLogo() {
        Image image = new Image(getClass().getResourceAsStream("/img/logo.png"));
        logo.setImage(image);
    }

    @FXML
    public void logIn(ActionEvent event) throws IOException {
        this.currentStage = (Stage) logInButton.getScene().getWindow();
        Association association = associationService.getByName((String) associationChoiceBox.getValue());
        if (association == null) {
            ShowErrorAuthDialog("Aucune association", NO_ASSOCIATION_SELECTED_MESSAGE);
        }
        else if (userService.checkCredentials(association, usernameField.getText(), passwordField.getText())) {
            hideCurrentStage();

            new HomeController().showHomePage(context);
        } else {
            ShowErrorAuthDialog("Mauvais identifiants", BAD_CREDENTIALS_MESSAGE);
        }
    }

    @FXML
    public void createAccount(ActionEvent event) throws IOException {
        this.currentStage = (Stage) signInButton.getScene().getWindow();
        Association association = associationService.getByName((String) associationChoiceBox.getValue());
        String firstName = firstNameField.getText();
        String lastName = lastNameField.getText();
        String password = createPasswordField.getText();
        String rank = (String) rankChoiceBox.getValue();

        if (association == null) {
            ShowErrorAuthDialog("Aucune association", NO_ASSOCIATION_SELECTED_MESSAGE);
        }
        else if (StringUtils.isAnyBlank(firstName, lastName, password)){
            ShowErrorAuthDialog("Au moins un champ vide", EMPTY_REQUEST);
        }
        else if(!userService.exists(firstName, firstName)) {
            boolean success = userService.create(association, rank, firstName, lastName, password);
            if(success){
                showAuthScene();
            } else {
                ShowErrorAuthDialog("Impossible de créer un compte", CREATION_FAILED);
            }
        } else {
            ShowErrorAuthDialog("Compte déjà existant", USER_ALREADY_EXIST);
        }
    }

    @FXML
    private void showSignInSceneListener(ActionEvent event) throws IOException {
        this.currentStage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        showSignInScene();
    }

    @FXML
    public void showSignInScene() throws IOException {
        URL url = getClass().getResource("/gsa/erp/view/signIn.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        fxmlLoader.setControllerFactory(context::getBean);
        Parent rootNode = fxmlLoader.load();

        setAssociationsData();
        setRankData();

        this.currentStage.setTitle("GSA - Création de compte");
        Scene scene = new Scene(rootNode);
        this.currentStage.setScene(scene);
    }

    @FXML
    public void showAuthSceneListener(ActionEvent event) throws IOException {
        this.currentStage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        showAuthScene();
    }

    private void showAuthScene() throws IOException {
        URL url = getClass().getResource("/gsa/erp/view/auth.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        fxmlLoader.setControllerFactory(context::getBean);
        Parent rootNode = fxmlLoader.load();

        setAssociationsData();
        setLogo();

        this.currentStage.setTitle("GSA - Connexion");
        Scene scene = new Scene(rootNode);
        this.currentStage.setScene(scene);
    }

    private void hideCurrentStage() throws IOException {
        this.currentStage.close();
    }

    private void ShowErrorAuthDialog(String errorName, String errorContent){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Erreur");
        alert.setHeaderText("Erreur - " + errorName);
        alert.setContentText(errorContent);
        alert.showAndWait();
    }
}
