package gsa.erp;

import gsa.erp.config.PersistenceConfig;
import gsa.erp.controller.AuthController;
import gsa.erp.model.repository.AffectationRepository;
import gsa.erp.service.EventService;
import gsa.erp.service.PlanningService;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.net.URL;

@Configuration
@EnableJpaRepositories
@SpringBootApplication
@Import(PersistenceConfig.class)
public class ErpApplication extends Application {

    private ConfigurableApplicationContext springContext;
    private Parent rootNode;

    public static void main(final String[] args){
        Application.launch(args);
    }

    @Override
    public void init() throws Exception {
        springContext = SpringApplication.run(ErpApplication.class);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        AffectationRepository affectationRepository = springContext.getBean(AffectationRepository.class);
        affectationRepository.findAll();
        URL url = getClass().getResource("/gsa/erp/view/auth.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        fxmlLoader.setControllerFactory(springContext::getBean);
        rootNode = fxmlLoader.load();
        AuthController authController = fxmlLoader.getController();
        authController.setAssociationsData();
        authController.setLogo();
        primaryStage.setScene(new Scene(rootNode));
        primaryStage.setTitle("GSA - Connexion");
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception{
        springContext.close();
    }
}
