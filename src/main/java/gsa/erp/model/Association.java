package gsa.erp.model;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "association")
@Transactional
public class Association {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_association",
            joinColumns = @JoinColumn(name = "user", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "association", referencedColumnName = "id"))
    private Set<User> members;

    @ManyToMany
    @JoinTable(
            name = "event_association",
            joinColumns = @JoinColumn(name = "event", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "association", referencedColumnName = "id")
    )
    private Set<Event> events;

    @Column(name = "address")
    private String address;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "embeddedId.association",cascade=CascadeType.ALL)
    private Set<UserAssociation> userAssociations = new HashSet<>(0);

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<User> getMembers() {
        return userAssociations.stream().map(UserAssociation::getUser).collect(Collectors.toSet());
    }

    public void setMembers(Set<User> members) {
        throw new UnsupportedOperationException();
    }

    public Set<Event> getEvents() {
        return events;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Association that = (Association) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
