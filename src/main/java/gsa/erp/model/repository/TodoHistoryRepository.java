package gsa.erp.model.repository;

import gsa.erp.model.TodoHistory;
import gsa.erp.model.TodoItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface TodoHistoryRepository extends JpaRepository<TodoHistory, Integer> {


}
