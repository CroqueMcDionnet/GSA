package gsa.erp.model.repository;

import gsa.erp.model.Activity;
import gsa.erp.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Integer> {
    Set<Activity> findByEvent(Event event);
}
