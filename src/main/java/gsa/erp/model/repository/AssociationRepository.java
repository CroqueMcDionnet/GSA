package gsa.erp.model.repository;

import gsa.erp.model.Association;
import gsa.erp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface AssociationRepository extends JpaRepository<Association, Integer> {

    @Transactional
    Association findById(int id);

}
