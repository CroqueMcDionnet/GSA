package gsa.erp.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user_association")
@AssociationOverrides({
        @AssociationOverride(name = "association",
                joinColumns = @JoinColumn(name = "association", referencedColumnName="association")),
        @AssociationOverride(name = "user",
                joinColumns = @JoinColumn(name = "user",referencedColumnName="user"))
})
public class UserAssociation {

    @EmbeddedId
    private UserAssociationId embeddedId = new UserAssociationId();

    @Column(name = "rank", nullable = false)
    @Enumerated(EnumType.STRING)
    private Rank rank;

    public enum Rank {
        ADMIN ("Administrateur"),
        MEMBER ("Membre");

        private String label;

        Rank(String label) {
            this.label = label;
        }

        public String getLabel() { return this.label; }

        public static Rank getByLabel(String value){
            Rank[] values = Rank.values();
            String enumValue = null;
            for(Rank eachValue : values) {
                enumValue = eachValue.getLabel();

                if (enumValue.equals(value)) {
                    return eachValue;
                }
            }
            return null;
        }
    }
    public User getUser() {
        return embeddedId.getUser();
    }

    public void setUser(User user) {
        embeddedId.setUser(user);
    }

    public Association getAssociation() {
        return embeddedId.getAssociation();
    }

    public void setAssociation(Association association) {
        embeddedId.setAssociation(association);
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    @Embeddable
    public static class UserAssociationId implements Serializable {
        @ManyToOne
        @JoinColumn(name = "user")
        private User user;

        @ManyToOne
        @JoinColumn(name = "association")
        private Association association;

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public Association getAssociation() {
            return association;
        }

        public void setAssociation(Association association) {
            this.association =  association;
        }
    }
}
