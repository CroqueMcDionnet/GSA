package gsa.erp.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    // hashed
    @Column(name = "password", nullable = false, length = 60)
    private String password;

    public User(int id, String firstName, String lastName, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }

    public User(){
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "embeddedId.user",cascade=CascadeType.ALL)
    private Set<UserAssociation> userAssociations = new HashSet<>(0);

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Affectation> affectations;

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getUsername() {
        return firstName + "." + lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<UserAssociation> getUserAssociations() {
        return userAssociations;
    }

    public void setUserAssociations(Set<UserAssociation> userAssociations) {
        this.userAssociations = userAssociations;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public Set<Affectation> getAffectations() {
        return affectations;
    }

    public void setAffectations(Set<Affectation> affectations) {
        this.affectations = affectations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return id == user.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
