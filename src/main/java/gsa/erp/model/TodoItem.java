package gsa.erp.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name= "todo_item")
public class TodoItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "todo_task", nullable = false)
    private String todoTask;

    @Column(name = "done")
    private boolean done;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private TodoItem.Type type;

    @Column(name = "owner", nullable = false)
    private int owner;

    @OneToMany(mappedBy = "todoItem")
    private List<TodoHistory> itemHistory;

    public List<TodoHistory> getItemHistory() {
        return itemHistory;
    }

    public enum Type {
        USER, ASSOCIATION
    }

    @Column(name = "checker")
    private String checker;

    public TodoItem() {}

    public TodoItem(String todoTask, boolean done, Type type, int owner) {
        this.todoTask = todoTask;
        this.done = done;
        this.type = type;
        this.owner = owner;
        this.checker = null;
    }

    public int getOwner() {
        return owner;
    }

    public int getId() {
        return id;
    }

    public String getTodoTask() {
        return todoTask;
    }

    public void setTodoTask(String todoTask) {
        this.todoTask = todoTask;
    }

    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    public boolean getDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "TodoItem{" +
                "id=" + id +
                ", todoTask='" + todoTask + '\'' +
                ", done=" + done +
                ", type=" + type +
                ", owner=" + owner +
                '}';
    }
}
