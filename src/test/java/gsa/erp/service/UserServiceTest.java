package gsa.erp.service;

import gsa.erp.model.Association;
import gsa.erp.model.User;
import gsa.erp.model.UserAssociation;
import gsa.erp.model.repository.UserAssociationRepository;
import gsa.erp.model.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private UserAssociationRepository userAssociationRepository;

    @InjectMocks
    private UserService userService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void create() throws Exception {

        User testUser = new User();
        UserAssociation testUserAssociation = new UserAssociation();
        Association testAssociation = new Association();

        testAssociation.setName("test");
        testAssociation.setEvents(Collections.emptySet());
        testAssociation.setAddress("120 rue du pied");
        testAssociation.getMembers().add(testUser);

        testUser.setFirstName("jean");
        testUser.setLastName("le sale");
        testUser.setPassword("$2a$12$xSDwWA3y4sp1NAF6K0HbK.3d.cZdwEEWoFGMi5Yo3dxvOBujLx3xK");
        testUser.setUserAssociations(Collections.singleton(testUserAssociation));

        testUserAssociation.setUser(testUser);
        testUserAssociation.setAssociation(testAssociation);
        testUserAssociation.setRank(UserAssociation.Rank.ADMIN);

        when(userRepository.save(any(User.class))).thenReturn(testUser);
        when(userRepository.findByFirstNameAndLastName("jean", "le sale")).thenReturn(Collections.singletonList(testUser));
        when(userAssociationRepository.save(any(UserAssociation.class))).thenReturn(testUserAssociation);

        boolean created = userService.create(testAssociation, UserAssociation.Rank.ADMIN.getLabel(),
                testUser.getFirstName(), testUser.getLastName(), "wx");

        Assert.assertTrue(created);
    }
}